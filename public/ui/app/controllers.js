function ListaComprasController($scope, $http) {

    $scope.item = [];

    $scope.itens = [];

	$scope.now = new Date();
	$scope.now.toISOString();

	console.log('Now: '+$scope.now);
	
	$scope.fetchProductsList = function() {
    		$http.get('http://telematics.localhost:8000/api/v1/devices').success(function(response){
				$scope.devices = response.data;
				angular.forEach($scope.devices, function(device){
				   $scope.itens.push({id: device.id, label: device.label, status: device.status, reported_on: device.updated_at, comprado: false});
                })
        		//toastr.success("Success fetching device.");
    		}).error(function(data) {
        		toastr.error("Fail on fetching device.");
    		});
		}

    $scope.adicionaItem = function () {
		$scope.newnow = new Date();
		$scope.newnow.toISOString();
		var data = $.param({
                label: $scope.item.label,
                status: $scope.item.status
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
		$http.post('http://telematics.localhost:8000/api/v1/devices', data, config).success(function(device) {
        	toastr.success("Device added successfully");
			$scope.itens.unshift({id: device.id, label: $scope.item.label, status: $scope.item.status, reported_on: $scope.newnow, comprado: false});
			$scope.item.label = $scope.item.status = '';
    	}).error(function(data) {
        	toastr.error("Fail on adding device.");
    	});
    };

    $scope.resetItem = function(index){
        $scope.item = {};
        $scope.edit = false;
    };

    $scope.editarItem = function(index){
        $scope.item = $scope.itens[index];
        $scope.edit = true;
		$scope.edit_id = $scope.item.id;
    };

    $scope.applyChanges = function(index){		
        $scope.edit = false;     
		var data = $.param({
                label: $scope.item.label,
                status: $scope.item.status
            });
        var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }
		$scope.item = {};
		$http.put('http://telematics.localhost:8000/api/v1/devices/' + $scope.edit_id, data, config).success(function(device) {
        	toastr.success("Device updated successfully");
			$scope.item.label = $scope.item.status = '';
    	}).error(function(data) {
        	toastr.error("Fail on updating device.");
    	});
		$scope.edit_id = 0;
    };

    $scope.deleteItem = function(index){
		$device_detail =  $scope.itens[index];       
		$scope.itens.splice(index, 1);
		$http.delete('http://telematics.localhost:8000/api/v1/devices/' + $device_detail.id).success(function (data, status) {
            toastr.success("Device deleted successfully");
        }).error(function(data) {
        	toastr.error("Fail on deleting device.");
    	});
    };
}
ListaComprasController.$inject = ['$scope', '$http'];