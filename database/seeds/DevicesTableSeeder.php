<?php

use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('devices')->truncate();
    	$now = date('Y-m-d H:i:s');
    	DB::table('devices')->insert([
            'label' => 'Device One 111',
            'status' => 'ok',
            'created_at' => '2018-01-08 10:55:37',
            'updated_at' => '2018-01-08 10:55:37',
        ]);
    	DB::table('devices')->insert([
            'label' => 'Device Two 222',
            'status' => 'ok',
            'created_at' => '2018-01-09 08:45:42',
            'updated_at' => '2018-01-09 08:45:42',
        ]);
    	DB::table('devices')->insert([
            'label' => 'Device Three 333',
            'status' => 'offline',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    	DB::table('devices')->insert([
            'label' => 'Device Four 444',
            'status' => 'ok',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    	DB::table('devices')->insert([
            'label' => 'Device Five 555',
            'status' => 'ok',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
