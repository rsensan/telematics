@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Telematics - Device Details</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('devices.index') }}"> Back</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Device Label:</strong>
                {{ $device->label}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
		        @if ($device->status == 'ok')
		        	<span class="text-success"><strong>{{ strtoupper($device->status) }}</strong></span>
		        @else
		        	<span class="text-danger"><strong>{{ strtoupper($device->status) }}</strong></span>
		        @endif
            </div>
        </div>
    </div>
@endsection