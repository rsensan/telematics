@extends('layouts.default')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Telematics - Devices</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('devices.create') }}"> Create New Device</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th>Device ID</th>
            <th>Device Label</th>
            <th>Status</th>
            <th>Reported On</th>
            <th width="280px">Actions</th>
        </tr>
    @foreach ($devices as $device)
    <tr>
        <td>{{ $device->id}}</td>
        <td>{{ $device->label}}</td>
        @if ($device->status == 'ok')
        	<td><p class="text-success"><strong>{{ strtoupper($device->status) }}</strong></p></td>
        @else
        	<td><p class="text-danger"><strong>{{ strtoupper($device->status) }}</strong></p></td>
        @endif
        <td>{{ $device->updated_at}}</td>
        <td>
            <a class="btn btn-info" href="{{ route('devices.show',$device->id) }}">Show</a>
            <a class="btn btn-primary" href="{{ route('devices.edit',$device->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['devices.destroy', $device->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </td>
    </tr>
    @endforeach
    </table>
    {!! $devices->render() !!}
@endsection