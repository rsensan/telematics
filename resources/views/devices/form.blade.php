<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong><label for="label">Device Label:</label></strong>
            {!! Form::text('label', null, array('placeholder' => 'Enter Device Label','class' => 'form-control','id' => 'label')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong><label for="ok">Device Status:</label></strong>
            {{ Form::radio('status', 'ok' , true, ['id' => 'ok']) }} <span class="text-success"><strong><label for="ok">OK</label></strong></span>
  			{{ Form::radio('status', 'offline' , false, ['id' => 'offline']) }} <span class="text-danger"><strong><label for="offline">OFFLINE</label></strong></span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>