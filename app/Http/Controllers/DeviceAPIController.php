<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Device;
use DB;
class DeviceAPIController extends Controller
{
    public function index()
    {
    	return $devices = Device::select(DB::raw("id, label, updated_at, CASE WHEN DATE_ADD(updated_at, INTERVAL 1 DAY) < NOW() THEN 'Offline' ELSE 'Ok' END  status"))->latest()->paginate(25);
//        $devices = Device::latest()->paginate(10);
//        return view('devices.index',compact('devices'))
//            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('devices.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'label' => 'required',
            'status' => 'required',
        ]);
        $device = Device::create($request->all());
        return response()->json($device, 201);
//        return redirect()->route('devices.index')
//                        ->with('success','Device created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Device $device)
    {
        return view('devices.show',compact('device'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Device $device)
    {
        return view('devices.edit',compact('device'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Device $device)
    {
        request()->validate([
            'label' => 'required',
            'status' => 'required',
        ]);
        $device_detail = $device->update($request->all());
        return response()->json($device_detail, 200);
//        return redirect()->route('devices.index')
//                        ->with('success','Device updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Device::destroy($id);
         return response()->json(null, 204);
//        return redirect()->route('devices.index')
//                        ->with('success','Device deleted successfully');
    }
}